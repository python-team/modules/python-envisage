python-envisage (7.0.3-2) unstable; urgency=medium

  * Team upload.
  * add build dependency on python3-configobj (Closes: #1082370)

 -- Alexandre Detiste <tchet@debian.org>  Sat, 21 Sep 2024 13:58:02 +0200

python-envisage (7.0.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 7.0.3
  * add build dependency on pybuild-plugin-pyproject
  * remove extraneous dependency on python3-six
  * remove old patch, not needed anymore

 -- Alexandre Detiste <tchet@debian.org>  Thu, 01 Feb 2024 08:35:06 +0100

python-envisage (6.1.0-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * New upstream version
    Closes: #1027612
  * Standards-Version: 4.6.2 (routine-update)
  * Drop unused include-binaries

  [ Scott Talbert ]
  * Fix test egg generation
  * Remove egg generation from d/rules as this is done by upstream now

 -- Andreas Tille <tille@debian.org>  Thu, 12 Jan 2023 07:16:16 +0100

python-envisage (6.0.1-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Andreas Tille ]
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Testsuite: autopkgtest-pkg-python (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * watch file standard 4 (routine-update)
  * Versioned Build-Depends: python3-traitsui (>= 7)
  * Do not point Homepage to pypi
  * Point watch file to Github
  * Fix build time test by preparing test eggs for Python 3.10
    Closes: #1002325

 -- Andreas Tille <tille@debian.org>  Mon, 14 Feb 2022 16:37:12 +0100

python-envisage (4.9.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * [a090e2a] Add egg-files for python3.9. (Closes: #973174)

 -- Anton Gladky <gladk@debian.org>  Mon, 30 Nov 2020 22:36:59 +0100

python-envisage (4.9.0-2) unstable; urgency=medium

  * Team upload.
  * Add envisage.egg-info/SOURCES.txt to debian/clean, to fix build twice
    in a row (closes: #671324).

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 30 Dec 2019 11:11:48 +0300

python-envisage (4.9.0-1) unstable; urgency=medium

  * Team Upload.

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * Remove debian/pycompat, it's not used by any modern Python helper
  * Convert git repository from git-dpm to gbp layout

  [ Scott Talbert ]
  * Remove outdated orig-tar.sh
  * Update to latest upstream release 4.9.0
  * Switch from Python 2 to Python 3 (Closes: #937730)
  * Update and convert d/copyright to DEP5 format
  * Use secure URL in d/watch

 -- Scott Talbert <swt@techie.net>  Thu, 26 Dec 2019 10:56:49 -0500

python-envisage (4.4.0-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Varun Hiremath ]
  * New upstream release
  * Bump Standards-Version to 3.9.5

 -- Varun Hiremath <varun@debian.org>  Sat, 15 Mar 2014 23:41:54 -0400

python-envisage (4.1.0-2) unstable; urgency=low

  * Fix typo in Breaks and Replaces (Closes: #668459)
  * Add Suggests: python-apptools, python-chaco (Closes: #668460)
  * Fix Vcs-{Svn, Browser} URLs

 -- Varun Hiremath <varun@debian.org>  Tue, 24 Apr 2012 19:08:44 -0400

python-envisage (4.1.0-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.3

 -- Varun Hiremath <varun@debian.org>  Mon, 23 Apr 2012 15:56:40 -0400

python-envisage (4.0.0-1) unstable; urgency=low

  * New upstream release
  * Rename package to python-envisage - replaces python-envisagecore
    and python-envisageplugins packages
  * Update d/control, d/copyright and d/watch

 -- Varun Hiremath <varun@debian.org>  Sat, 09 Jul 2011 00:05:01 -0400

python-envisagecore (3.2.0-2) unstable; urgency=low

  * Team upload.
  * Rebuild to pick up 2.7 and drop 2.5 from supported versions
  * d/control: Bump Standards-Version to 3.9.2

 -- Sandro Tosi <morph@debian.org>  Sat, 07 May 2011 14:47:58 +0200

python-envisagecore (3.2.0-1) unstable; urgency=low

  * New upstream release
  * Convert to dh_python2 (Closes: #617004)

 -- Varun Hiremath <varun@debian.org>  Tue, 05 Apr 2011 23:54:28 -0400

python-envisagecore (3.1.3-1) experimental; urgency=low

  * New upstream release
  * d/control: Bump Standards-Version to 3.9.1

 -- Varun Hiremath <varun@debian.org>  Sun, 17 Oct 2010 21:33:28 -0400

python-envisagecore (3.1.2-1) unstable; urgency=low

  * New upstream release
  * Switch to source format 3.0
  * Bump Standards-Version to 3.8.4

 -- Varun Hiremath <varun@debian.org>  Sun, 28 Feb 2010 14:35:30 -0500

python-envisagecore (3.1.1-2) unstable; urgency=low

  * debian/control:
    - Add python-pkg-resources to Depends (Closes: #560724)
    - Bump Standards-Version to 3.8.3

 -- Varun Hiremath <varun@debian.org>  Tue, 15 Dec 2009 10:47:32 -0500

python-envisagecore (3.1.1-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.8.2

 -- Varun Hiremath <varun@debian.org>  Mon, 20 Jul 2009 03:15:07 -0400

python-envisagecore (3.1.0-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Thu, 26 Mar 2009 20:03:16 -0400

python-envisagecore (3.0.1-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Varun Hiremath ]
  * New upstream release
  * debian/control: add python-setupdocs to Build-Depends

 -- Varun Hiremath <varun@debian.org>  Sun, 28 Dec 2008 22:38:20 -0500

python-envisagecore (3.0.0-1) experimental; urgency=low

  * Initial release

 -- Varun Hiremath <varun@debian.org>  Sun, 26 Oct 2008 01:36:05 -0400
